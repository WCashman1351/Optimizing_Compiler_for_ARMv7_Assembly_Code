# Optimizing Compiler for ARMv7 Assembly Code

## Description

Performs four different peephole optimizations on ARMv7 assembly code to improve runtime of programs. 

Two improve runtime by reducing the number of lines of code to be executed: 
* Constant Folding
* Conditional statement optimization (using IT Blocks)

Two improve runtime by reducing control statements at the expense of memory: 
* Counted loop unrolling
* Function Inlining
    
The first two can be used in any situation. The second two should only be used when memory is in excess

### Prerequisites 

A Python 3.6.x console that can read .py files.


### Useage 

Open file in Python 3.6.x kernel.
Insert the code that you wish the optimizations to be performed on into a text file.
Run the program and input the file path of the text file are prompted by the terminal.
The output will be displayed on the terminal.

Assumes: 
* A text file as input
* Syntactically correct ARMv7 Assembly code.

The 'Example.txt' file has been included in this folder as an example of a correct input file.

If only a few of the optimizations are to be performed, comment the lines a the bottom of the code pertaining to the optimizations that you don't want performed

                instructions, labelDict = insertITBlocks(instructions, labelDict)
                instructions, labelDict = inlineFunctions(instructions, labelDict, deleteInlinedFunc=True)
                instructions, labelDict = unrollingForLoops(instructions, labelDict)
                instructions, labelDict = constantFolding(instructions)             

By default, all of the optimizations will be carried out.

To specify the maximum number of loop unrollings you want to take place, manually input the number as a third parameter in the 'unrollingForLoops' function. 

### Credits: William Cashman 2018

