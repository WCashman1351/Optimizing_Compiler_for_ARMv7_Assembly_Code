''' A basic optimizing compiler for ARMv7 Assembly Code.
    Performs four optimization passes:
        - Constant Folding
        - Counted Loop Unrolling
        - Condition execution with IT blocks
        - Function Inlining
    Assumes syntactically correct code as input
    <> Will Cashman 2018 <>'''

import math

        ### Gets the Input File Path ###

options = {'terminal', 'file', 'both'};  output = ''; validInput = False
# Waits for a valid file path
while not validInput:
    try:
        inputFilePath = input("Input File Path: ")
        inputFileObj = open(inputFilePath, 'r') 
        validInput = True
    except FileNotFoundError:
        print('Not a valid File Path. Try again')
        validInput = False
# Reads the instructions from the file and saves it to the "instructions" variable
instructions = inputFileObj.readlines()
inputFileObj.close()

              
        ### Initial Scrub ###

# Removes whitespace, comments, cnverts all characters to lowercase, and converts all numbers to decimal
line = 0;  labelDict = {};  prefixes = {'#', '0'};
while line < len(instructions):
    instr = instructions[line]
    # Removes comments
    if instr.find('@') != -1:
        instr = instr[:instr.find('@')]
    # Remove whitespace
    instr = instr.strip() 
    
    if instr == '':
    # Remove any blank lines or comments   
        instructions.pop(line)
        continue                                   
    elif instr[-1] == ':':
    # Save line number of labels to labelDict
        labelDict[instr[:-1]] = line   
    else:
    # Convert the operation type to lowercase, and convert numbers to decimal
        # Records the format the instruction is in and saves it to instrFormat
        instrFormat = ''; newBit = True
        for i in instr:
            if i == ' ' or i == ',':
                instrFormat += i
                newBit = True
            else:
                if newBit == True:
                    instrFormat += '{}'
                    newBit = False
        instr = instr.replace(',', ' ').split()
        instr[0] = instr[0].lower()
        # Converts numbers to their decimal equivalents
        i = 1
        while i < len(instr):
            newInstr = instr[i]
            if newInstr[0] == 'r':
                i += 1
                continue
            elif newInstr[0] == '#':
                instr[i] = newInstr[1:]
            elif newInstr[0:2] == '0x':
                instr[i] = str(int(newInstr[2:],16))
            elif newInstr[0:2] == '0b':
                instr[i] = str(int(newInstr[2:],2))
            i += 1
        instr = instrFormat.format(*instr) # Reinserts the instruction back into its original format
    instructions[line] = instr             # Saves the modified instruction back to the original instruction list       
    line +=1

####################################################################################################

def recalibrateLabelDict(instructions):
    ''' Saves the labels in the instructions list as keys in labelDict, with their corresponding line numbers as the values
        Also removes any empty lines in the process'''
    line = 0;  LabelDict = {};
    while line < len(instructions):
        if instructions[line] == '': 
        # Remove any blank lines                     
            instructions.pop(line)
            continue                                  
        elif instructions[line][-1] == ':':
        # Save line number of labels to labelDict
            LabelDict[instructions[line][:-1]] = line   
        line +=1
    return instructions, LabelDict

####################################################################################################

def inlineFunctions(instructions, labelDict):
    ''' Inlines all the function calls in the 'instructions' parameter.
        Assumption: - Every function is delimited by the "bx lr" instruction
                    - Every function contains only one "bx lr" instruction  '''
    
    usedFunctions = dict(); line = 0;
    while line < len(instructions):
        if instructions[line][:2] == 'bl':                    # Finds a function call   
            functionLabel = instructions[line][3:]         
            if functionLabel not in labelDict:
                line += 1
                continue
            funcLine = labelDict[functionLabel]
            
            # Gets the body of the function and saves it to the 'funcBody' variable
            if functionLabel in usedFunctions:
                # Retrieve length of the function body from a dictionary
                funcBody = usedFunctions[functionLabel]                  
            else:    
                # Manually calculate the length of the function body
                count = 1;  funcBody = [];
                while instructions[funcLine + count] != 'bx lr':
                    funcBody.append(instructions[funcLine + count])
                    count += 1
            # Inserts the function body into the main code and quickly recalibrates the labelDict values
            instructions = instructions[:line] + funcBody + instructions[line+1:] 
            line += len(funcBody) -1
            for key in labelDict:
                if labelDict[key] > line:
                    labelDict[key] += len(funcBody) - 1
            # If the function has not been inlined before, stores the body in a dictionary for faster execution
            if functionLabel not in usedFunctions:
                usedFunctions[functionLabel] = funcBody    
        line += 1
        
    # Deletes the functions that were inlined at some point
    for func in usedFunctions:  
        funcLine = labelDict[func]
        while instructions[funcLine] != 'bx lr':
            instructions[funcLine] = ''                 # Preserves the line number, as opposed to popping the instruction
            funcLine += 1
        instructions[funcLine] = ''
                
    return recalibrateLabelDict(instructions)

        ## Template for Testing ##
#instructions, labelDict = inlineFunctions(instructions, labelDict) 
#for i in instructions:
#    print(i)

####################################################################################################

def unrollingForLoops(instructions, loopDict, maxUnrollLoop=0):
    ''' Unrolls all the for loops and returns a list of the modified instructions.
        Valid Loop: - Loop is terminated by either a "bne" or "cbnz" branch
                    - The flag setting instruction is either a "subs" or "cmp" with immediate values
                    - The counting register is increments by "add" or "sub" instructions with immediate values
                    - The counting register's value is setting before the loop by a "mov" or "ldr"
                      with an immediate value, and not before any branches'''
                      
    tests = {'cmp', 'tst', 'cmn', 'teq'};  ops = {'add':'+', 'sub':'-'}; # Initialize useful data structures
    for Label in loopDict:    
        labelLine = loopDict[Label];  increment = 0;  skipLines = []
        
        ## Finds the line number of the end of the loop and the last cmp instruction 
        loopLine = labelLine + 1
        while loopLine < len(instructions): 
            loopInstr = instructions[loopLine]
            if Label in loopInstr:
                # Only supports 'cbz' or 'beq' terminations
                if loopInstr[:3] == 'cbnz':
                    counterReg = loopInstr[1]
                    endPoint = 0    
                elif loopInstr[:3] != 'bne':
                    loopLine = len(instructions)
                skipLines.append(loopLine)
                endLine = loopLine
                break    
            loopLine += 1
        # Either not a loop, or the loop does not meet our requirements
        if loopLine == len(instructions):
            continue   

        # Determines how the loop tests for termination
        endPoint = 'Not found'
        while loopLine > labelLine:
            loopInstr = instructions[loopLine].replace(',',' ').split()
            if loopInstr[0] == 'subs':
                # Checks that the counter is being decremented by a constant rather than a register
                if loopInstr[-1].isdigit() and loopInstr[-2][0] != 'l':
                    counterReg = loopInstr[1]
                    endPoint = 0
                break
            elif loopInstr[0] == 'cmp':
                # Checks that the compare is done with an immediate value rather than a register
                if loopInstr[2].isdigit():   
                    counterReg = loopInstr[1]   
                    endPoint = int(loopInstr[2])
                    skipLines.append(loopLine)
                break
            loopLine -= 1
        # Not a valid loop
        if endPoint == 'Not found':
            continue
       
        # Determines how the program is incrementing the count register
        removeIncrement = True;  incrementLines = [];
        while loopLine > labelLine:
            instr = instructions[loopLine].replace(',', ' ').split()
            if counterReg in instr:
                if instr[1] == counterReg and instr[0] not in tests:
                    # The instruction is modifying the counter register
                    if instr[0][:3] in ops and loopInstr[-1].isdigit() and loopInstr[-2][0] != 'l':
                        # The operation is an ADD or SUB instruction with immediate value and is therefore valid
                        increment = int(eval(str(increment) + ops[instr[0][:3]] + instr[-1]))
                        incrementLines.append(loopLine)
                    else:
                        # The operation is modifying the counting register but is invalid
                        increment = 0
                        break     
                else:
                    # The instruction uses the counter register so we cannot delete the increments
                    removeIncrement = False
            loopLine -= 1
        # No net increment takes places or no increment was found  
        if increment == 0:
            continue    
        # If it is okay for us the remove the lines that increment the lines, then do so
        if removeIncrement:
            skipLines.extend(incrementLines)
        
        # Find the initial value of the counting register
        initialVal = '0'; loopLine = labelLine-1;
        while loopLine >= 0:
            instr = instructions[loopLine]
            if counterReg in instr:
                instr = instr.replace(',',' ').split()
                if instr[1] == counterReg and (instr[0] == 'mov' or instr[0] == 'ldr'):
                # Instruction must be of the form: ldr/mov Rc, i;  Rc = counting register, i is immediate value   
                    if instr[2].isdigit():
                        initialVal = int(instr[2])
                        break
            elif instr[0] == 'b' or instr[-1] == ':':
            # If there is a branch or extra label then this hints at an indeterminate initial value  
                break
            loopLine -= 1
        # Initial value not found
        if type(initialVal) == str:
            continue
        
        # Calculates the number of times to unroll the loop, then saves the body of the loop in the 'loopBody' variable
        numLoopIter = math.ceil(abs((endPoint-initialVal) / increment)) 
        loopBody = [];  loopLine = labelLine + 1
        while loopLine < endLine:
            if loopLine not in skipLines:
                loopBody.append(instructions[loopLine])
            loopLine += 1  
        # Checks to see if the unrolled loop complies with the maximum number loop unrollings specified
        if maxUnrollLoop != 0 and numLoopIter > maxUnrollLoop:
            continue
        
        # Multiplies the 'loopBody' variable to unroll the loop, but changes the labels in the body to avoid error
        total = []
        for count in range(numLoopIter):
            newList = []
            i = 0
            while i < len(loopBody):    
                if loopBody[i][-1] == ':':
                    # Changes any labels
                    newList.append(loopBody[i][:-1] + str(count) + ':')
                else:
                    newList.append(loopBody[i])
                i += 1
            total.extend(newList)
            
        # Inserts the unrolled loop back into the main code and quickly recalibrates the label dictionary
        if removeIncrement:
            # Leaves the counter register with the same value as with the loop
            instructions = instructions[:labelLine+1] + total + ['mov ' + counterReg + ', ' + str(endPoint)] + instructions[endLine+1:]
        else:
            instructions = instructions[:labelLine+1] + total + instructions[endLine+1:]
        for labels in loopDict:
            if loopDict[labels] > labelLine:
                loopDict[labels] += (numLoopIter-1) * len(loopBody)  - len(skipLines)
    
    return recalibrateLabelDict(instructions)        

           ## Template for Testing ##
#instructions, labelDict = unrollingForLoops(instructions, labelDict) 
#for i in instructions:
#    print(i)

####################################################################################################
         
def insertITBlocks(instructions, labelDict):
    ''' Changes an if statement into an IT block statement for optimization purposes.
        Requirements: - Must have a conditional branch
                      - The label that is branched to THEN statement must end up at 
                        the same place as the instructions after the ELSE statement.
                      - The total amount of lines in the THEN and ELSE statements put 
                        together, must be equal to or less than four.'''           
       
    negateCondCodes = {'eq':'ne', 'ne':'eq', 'hs':'lo', 'cs':'cc', 'lo':'hs', 'cc':'cs', 'mi':'pl', 'pl':'mi', 'vs':'vc', 'vc':'vs', \
                      'hi':'ls', 'ls':'hi', 'ge':'lt', 'lt':'ge', 'gt':'le', 'le':'gt', 'al':''}
    line = 0
    while line < len(instructions):
        ## Checks for a conditional branch
        if instructions[line][1:3] in negateCondCodes:
            TLineNum = labelDict[instructions[line].split()[-1]] + 1    # The line number of the first instruction in the THEN statement
            ELineNum = line+1                                           # The line number of the first statement in the ELSE clause
                
            # Initializes many values
            count = 0;  startIndex = TLineNum; RestOfProg = None; validLoop =False; popBranch = 0;
            lineNumbers = [[TLineNum-1], []]; index = 0
            # Detemines the length of the IT Block.  # Note: This searches through both the THEN and ELSE statement in the one loop
            while count < 5 and (startIndex + count < len(instructions)): 
                newInstr = instructions[startIndex+count]
                lineNumbers[index].append(startIndex+count)
                if newInstr[0] == 'b':
                    newInstr = newInstr.split()[-1]
                elif newInstr[-1] == ':':
                    newInstr = newInstr[:-1]
                else:
                    # It isn't a branch statement nor a label
                    count += 1 
                    continue
                # It is a branch statement or a label
                if index == 1:
                    # It has reached the end of the ELSE statement
                    if newInstr == RestOfProg:
                        validLoop = True
                    break
                else:
                    index = 1
                # Now start looking through the ELSE statement
                RestOfProg = newInstr
                startIndex = ELineNum - count
                newInstr = instructions[ELineNum].split()
                if newInstr[0] == 'b':
                    if newInstr[1] == RestOfProg:
                        # If the ELSE statement is just a single branch then we are done
                        break
                    # If branching to a new label, go to that branch
                    popBranch = ELineNum
                    startIndex = labelDict[newInstr[1]] - count  + 1
                    lineNumbers[1].append(labelDict[newInstr[1]])
                thenLines = count
            # The conditional is not valid by our standards    
            if not validLoop:
                break
      
            # At this point: x + y <= 4, and both sides of the statement end up at the same place
            itBlock = [];  condCode = instructions[line][1:3];
            if popBranch != 0:
                # Removes the branch to the ELSE statement
                instructions[popBranch] = ''
            instructions[line] = ''             # Removes the branch to the THEN statement
            
            ## Organises the IT Block
                # THEN Statement
            for i in lineNumbers[0]:
                instr = instructions[i].split()
                if instr[0][-1] != ':' and instr[0] != 'b':
                    # If the instruction is not a label or branch, then add it to the IT Block
                    instr[0] = instr[0] + condCode
                    itBlock.append(' '.join(instr))
                if instr[0][-1] != ':':
                    # If the instructions is not a label, remove it from the instructions list
                    instructions[i] = ''
                # ELSE Statement
            for i in lineNumbers[1]:
                instr = instructions[i].split()
                if instr[0][-1] != ':' and instr[0] != 'b':
                    instr[0] = instr[0] + negateCondCodes[condCode]
                    itBlock.append(' '.join(instr))
                if instr[0][-1] != ':':
                    instructions[i] = ''
                # Deletes labels 
            for i in range(thenLines+2):
                if instructions[TLineNum+i-1][:-1] != RestOfProg:
                    instructions[TLineNum+i-1] = ''
            
            ## Implements the IT Block     
            instructions = instructions[:line] + ['i'+thenLines*'t'+(count-thenLines)*'e'+' '+condCode] + itBlock + instructions[line+1:]
            line += 1 + len(itBlock)
            while instructions[line] == '':
                line += 1
            if instructions[line][:-1] != RestOfProg:
                # Inserts a branch to the RestOfProg location if it does not naturally go there 
                instructions.insert(line, 'b ' + RestOfProg)
            instructions, labelDict = recalibrateLabelDict(instructions)
        line += 1
    
    return recalibrateLabelDict(instructions)

              ## Template for Testing ##             
#instructions, labelDict = insertITBlocks(instructions, labelDict, False) 
#for i in instructions:
#    print(i)
    
    
####################################################################################################

def constantFolding(instructions):
    ''' Precomputes or simplifies arithmetic expressions to make the code more efficient.
        Implements three main evaluations: 
            - Evaluation of constants e.g. 4 + 4 = 8
            - Replacing multiplicative instructions with barrel shifts
              e.g. Rn / 8 = Rn >>> 3 or (Rm * 8 and Rn + Rm  = Rn + (Rm <<< 3) 
            - Simple additive and multiplicative associativity rules
              e.g. 4 * Rn * 8 = 32 * Rn   or   4 + Rn + 8 = 12 + Rn'''
    
        # Initializes useful variables
    nodeDict = {};  listDict = {};  operations = {'add', 'sub', 'mul', 'udiv', 'sdiv', 'ldr', 'mov', 'lsl', 'lsr'}
    for i in range(13):
        listDict['r'+str(i)] = []
    
    def optimize(reg):
        ''' Implements a five stage procedure for optimizing the instructions by performing various
            graph transformations on the reg paramater's node tree. 
            Returns a list of the new (optimized) instructions to be written back into the file'''
        
        # Initialize useful dictionaries  # Note: With enough thought, the function could be restructured to use less data structures
        regTree = nodeDict[reg];  opSymb = {'add':'+', 'sub':'-', 'mul':'*', 'udiv':'/', 'sdiv':'/'}
        powersOfTwo = dict((2**(i),i) for i in range(-31, 31));  nullValue = {'multiplier':1, 'additive':0}
        opClass = {'add':'additive', 'sub':'additive', 'mul':'multiplier', 'udiv':'multiplier', 'sdiv':'multiplier', 'mov':'move'}
        transform = {'lsr':'udiv', 'lsl': 'mul'};  multipliers = {'mul':'lsl', 'udiv':'lsr', 'sdiv':'lsr'}
        
        ## Phase 1 - Evaluating Constants ##
        current = regTree.first
        while current.parent != 'Root':
            current = current.parent
            # Checks to see if the two leaf nodes are integers and can therefore be evaluated
            if type(current.left.value) == int and type(current.right.value) == int:
                if current.reversed == True:
                    current.value = int(eval(str(current.right.value) + opSymb[current.value] +str(current.left.value)))
                else:
                    current.value = int(eval(str(current.left.value) + opSymb[current.value] +str(current.right.value)))
                regTree.first = current
                del current.left 
                del current.right
                regTree.setMultiple(current, left=None, right=None, formerID='')
            else:
                break  
        
        # If the tree is now just a single node then return that single value
        if regTree.checkForSingleNode():
            return regTree.checkForSingleNode()   
        
        # Ensure that in all cases the left node is either an operation or a register (not a register)
        firstPar = regTree.first.parent
        if type(firstPar.left.value) == int or firstPar.right.value == regTree.treeID or firstPar.right.formerID == regTree.treeID:                 
            # Swaps the two leaf nodes
            regTree.setMultiple(firstPar, left=firstPar.right, right=firstPar.left )               
            regTree.first = firstPar.left
            firstPar.left.ID = 'L0'
            firstPar.right.ID = 'R0'
            if not (firstPar.value ==  'mul' or firstPar.value == 'add'):
                # If its a SUB or DIV instruction, then we need to indicate that we have reversed it
                firstPar.reversed = 1^int(firstPar.reversed)
        

        ## Phase 2 - Expanding shifts ##
        current = regTree.first
        while current != 'Root':
            if current.shift != 0:
                # Expands a barrel shift in one instruction into a separate MUL or DIV instruction
                if current.shift > 0:
                    regTree.addNode('mul', (2**(current.shift), ''), 0, 0, current.left.ID[1:])
                else:
                    regTree.addNode('udiv', (2**abs(current.shift), ''), 0, 0, current.left.ID[1:])
                current.shift = 0
                regTree.recalibrateNodeIDs()
            if current.value in transform:
                # Transforms an LSL or LSR instruction into its equivalent MUL or DIV instruction 
                current.value = transform[current.value]
                current.right.value = 2**(current.right.value)
            current = current.parent
        
        ## Phase 3 - Simple Commutativity Transformations ##   
        current = regTree.first.parent 
        while current.parent != 'Root':
            if (opClass[current.value] == opClass[current.parent.value]) and (type(current.right.value) == int):  
                currentOpClass = opClass[current.value]   # Denotes the operation class we are currently working in, either: additive(ADD and SUB) or multiplicative(MUL and DIV)
                nodeCounter = current
                # These conditions indicate whether we are staying in the same operation class and have valid instructions to operate on
                while nodeCounter.parent != 'Root' and currentOpClass == opClass[nodeCounter.parent.value] and not nodeCounter.reversed:
                    nodeCounter = nodeCounter.parent
                    if type(nodeCounter.right.value) != int:
                        continue
                    
                    # Evaluates the result of the two nodes
                    evalValue = eval(str(nullValue[opClass[current.value]]) + opSymb[current.value] +str(current.right.value) + opSymb[nodeCounter.value]+str(nodeCounter.right.value))
                    if currentOpClass == 'additive':
                        # Checks if evalValue is a valid immediate value
                        testVal = evalValue
                        while testVal % 4 == 0:
                            testVal /= 4
                        if testVal >= 256:
                            continue
                    elif evalValue not in powersOfTwo:
                        continue
                    # At this point we can safely change the nodes to the evaluated value
                    regTree.deleteNode(nodeCounter.ID)
                    if nullValue[currentOpClass] == evalValue:
                        # This is the case where the two operations cancel each other out
                        regTree.deleteNode(current.ID)
                        if current.left != None:
                            current = current.left
                        elif current.parent != 'Root':
                            current = current.parent
                        break
                    else:
                        # Sets the node's value to the new evaluated value
                        regTree.setMultiple(current.right, value=evalValue, formerID='')              
            else:
                current = current.parent    
        
        ## Phase 4 - Collapsing Shifts ##
        current = regTree.first.parent
        while current != 'Root':
            if current.value not in multipliers or (current.right.value not in powersOfTwo):
                # Does not form a valid sequence on instructions
                current = current.parent
                continue            
            if current.parent != 'Root':
                if type(current.parent.right.value) == str and (current.parent.value == 'add' or ( current.parent.value == 'sub' and current.parent.reversed)):   
                # In this case we can inline the barrel shift in another instruction e.g. add r0, r1, r2, lsl 3   
                    if current.value == 'mul':
                        newShift = current.shift + powersOfTwo[current.right.value]
                    else:
                        newShift = current.shift - powersOfTwo[current.right.value]
                    if abs(newShift) < 8:
                        current.left.shift = newShift
                        regTree.deleteNode(current.ID)
                else:
                    # Turn the multiplier operation into an LSL or LSR operation
                    current.value = multipliers[current.value]
                    regTree.setMultiple(current.right, value=powersOfTwo[current.right.value], formerID='')
            else:
            # Turn the multiplier operation into an LSL or LSR operation
                current.value = multipliers[current.value]
                regTree.setMultiple(current.right, value=powersOfTwo[current.right.value], formerID='')
            current = current.parent
        
        regTree.recalibrateNodeIDs() # Phase 5 uses the node ID's and Phases 1, 2, 3 and 4 could have messed it up
        
        ## Phase 5 - Restoring FormerID's ##
        current = regTree.first.parent
        if current.value in multipliers:
            if type(current.left.value) == int:
                # We either need to restore the former ID, or add a MOV operation and store it
                if current.left.formerID == regTree.treeID:
                    regTree.addNode('mov', ['r',''], 0, 0, '1', False)
                else:
                    current.left.value = current.left.formerID
            if type(current.right.value) == int:
                # In this case we must reverse the position of the two leaf nodes because we can only insert the MOV node below the left node
                if current.right.formerID == regTree.treeID:
                    regTree.setMultiple(current, left=current.right, right=current.left )               
                    regTree.first = current.left
                    current.left.ID = 'L0'
                    current.right.ID = 'R0'
                    if not (current.value ==  'mul' or current.value == 'add'):
                        # If its a SUB or DIV instruction, then we need to indicate that we have reversed it
                        current.reversed = 1^int(current.reversed)
                    regTree.addNode('mov', ['r',''], 0, 0, '1', False)
                else:
                    current.right.value = current.right.formerID
        # Only checks "right" nodes
        while current != 'Root': 
            if current.value in multipliers and type(current.right.value) == int:
                current.right.value = current.right.formerID
            current = current.parent
        
        return regTree.exportNodesToList() # Exports the tree as a list of instructions

    
    ####################################################################
    def terminateTree(reg, addInstructions=True):
        ''' Runs optimizations on the tree for the 'reg' register, converts the
            tree back into ARM instructions, and then inserts the new instructions back into the main instruction list.
            If the "addInstructions" parameter is false, then the tree is still optimized but none
            instructions in the tree are written back into the code.'''  
        
        treeLayout = [];  nodeDict[reg].terminated = True;  count = 0;
        if addInstructions:
            # Runs the optimizations of the registers corresponding tree and saves them to 'treeLayout'
            treeLayout = optimize(reg)
        
        # Rewrites the lines in the 'instructions' list
        for line in listDict[reg]:
            if count < len(treeLayout):
                if len(treeLayout[count]) == 3:
                    # Case for MOV and LDR instructions
                    instructions[line] = ('{} {}, {}').format(*treeLayout[count])
                else:
                    # Case for the rest of the instructions
                    instructions[line] = ('{} {}, {}, {}{}').format(*treeLayout[count])
            else:
                # Clears any of the unused lines
                instructions[line] = ''
            count += 1
        listDict[reg] = []
        return
        
    ####################################################################
    def getSingleValue(reg):
        ''' Terminates the register's (parameter) tree and returns the single value of that tree (if it exists) alongside 
            the original register name (former ID) in a tuple.'''
        if reg in nodeDict:
            # Since the register is being used in an instruction, its tree must be terminated
            if not nodeDict[reg].terminated:
                terminateTree(reg)
            val = nodeDict[reg].checkForSingleNode()
            if val != False:
                # Tests to see if its a valid immediate value
                testVal = int(val[0][2])
                while testVal % 4 == 0:
                    testVal /= 4
                if testVal < 256:
                    return (int(val[0][2]), reg) # The single node value and the formerID value 
        # Register does not have a tree
        if reg.isdigit():
            reg = int(reg)
        return (reg, reg)
    
    ####################################################################
    
        ##### The Main Loop #####
        
    line = 0; addToListDict = True;
    while line < len(instructions):
        components = instructions[line].replace(',', ' ').split()
        # Only operates on the instructions specified in the 'operations' set
        if components[0] in operations:
            newRd = components[1] 
            # MOV Instruction
            if components[0] == 'mov':
                if newRd in nodeDict:
                    terminateTree(newRd, False)
                nodeDict[newRd] = Tree([getSingleValue(components[-1])[0],newRd], newRd)
            # LDR Instructions
            elif components[0] == 'ldr':
                if components[-1][1:].isdigit():   # ldr r1, =i where i is an constant
                    nodeDict[newRd] = Tree(components[-1][1:], newRd)
                else:
                # Since its being changed and we don't know what it is we have to terminate the tree      
                    componentsNew = instructions[line].replace(',', ' ')
                    componentsNew = componentsNew.replace('[', ' ')
                    for reg in componentsNew.replace(']', ' ').split():
                        if reg in nodeDict:
                            terminateTree(newRd, False) 
                    addToListDict = False               # We want this to remain in the instructions list
            else:     
            ## So now op = one of {add, mul, div, sub} and a different tree is starting/continuing
                if components[-2] == 'lsl':
                    shift = int(components[-1])
                elif components[-2] == 'lsr':
                    shift -1 * int(components[-1])
                else:
                    shift = 0
                ## If a tree does not exist, this bit just generates part of it, so the rest of the code can be used 
                if newRd not in nodeDict:
                    if len(components) == 3 or len(components) == 5:
                        nodeDict[newRd] = Tree(getSingleValue(newRd), newRd)
                    else:
                        nodeDict[newRd] = Tree(getSingleValue(components[2]), newRd)
                
                # Encodes the instruction into the appropriate node to add to the tree 
                if len(components) == 3 or len(components) == 5:  
                        nodeDict[newRd].addNode(components[0], getSingleValue(components[2]), shift)
                else:
                    if components[2] == newRd:
                        nodeDict[newRd].addNode(components[0], getSingleValue(components[3]), shift) 
                    elif components[3] == newRd:
                        nodeDict[newRd].addNode(components[0], getSingleValue(components[2]), 0, shift, reverse=True)       
                    else:   
                    ## New definition of Rd, in the form: op Rd, Rm, [Rn|i]. So we must create a new tree
                        if not nodeDict[newRd].terminated:
                        # If they haven't been terminated then the register must not have been used, so 
                        # theres no need to apply the useless instructions
                            terminateTree(newRd, False)
                        nodeDict[newRd] = Tree(getSingleValue(components[2]), newRd)
                        nodeDict[newRd].addNode(components[0], getSingleValue(components[3]), shift)
            ## Terminate the trees of the other registers that were used in that instruction
            for i in range(1,len(components)):
                if components[i] in nodeDict and components[i] != newRd:
                    if nodeDict[components[i]].terminated == False:
                        terminateTree(components[i])
            if addToListDict:
                # Updates the number of lines that change newRd 
                listDict[newRd].append(line) 
            else:                 
                addToListDict = True
        
        else:
            # Not a valid instruction, so it terminates every register that was used in that instruction
            for part in components:
                if part in nodeDict:
                    if nodeDict[part].terminated == False:
                        terminateTree(part)
            # If its a branch or a label, then the values of the register are indeterminate and so we must clear all trees
            if components[0][0] == 'b' or components[0][-1] == ':':
                for reg in nodeDict:
                    if nodeDict[reg].terminated == False:
                        terminateTree(reg)
                nodeDict = dict()
                # Reset listDict
                for i in range(13):
                    listDict['r'+str(i)] = []
        line += 1
    
    # Before the function exists, it must terminate all the trees to ensure that the values of the registers are left the
    # same as in the unoptimized code
    for reg in nodeDict:
        if nodeDict[reg].terminated == False:
            terminateTree(reg)
    
    return recalibrateLabelDict(instructions)

        #### General Framework ####

class Node(object):
    ''' The node object that is used in the Tree object.
        The 'formerID' attribute is used during the graph transformations to store previous values
        for the node'''
    def __init__(self, leftNode, rightNode, value, parent='Root', shift=0, formerID='', reverse=False):
        self.left = leftNode        # The node below and to the left of it
        self.right = rightNode      # The node below and to the right of it
        self.value = value          # The value of the node
        self.parent = parent        # The parent of the node
        self.shift = shift          # The barrel shift associated with the node
        self.formerID = formerID    # The nodes former value (i.e. a register was replaced with the evaluated value of that register)
        self.reversed = reverse     # Indicated whether or not the two nodes below it are in the reversed order

class Tree(object):
    
    def __init__(self, value, Reg):
        ''' Initialises the tree with a single element'''

        self.first = Node(None, None, value[0], 'Root', 0, value[1])
        self.current = self.first
        self.terminated = False
        self.treeID = Reg
        
    def addNode(self, relation, rightVal, rightShift=0, relationShift=0, parent='Root', reverse=False):
        ''' Creates an inserts a node below the node specified in the 'parent' parameter (via its ID).
            Also creates a right branch to that node.'''       
        
        current = self.first
        # Get the node immediately below it
        while current.parent != 'Root':
            if current.parent.ID[1:] == parent:
                 break
            current = current.parent
        
        # Create and insert the nodes
        if parent[1:] != '0': 
            rightNode = Node(None, None, rightVal[0], None, rightShift, rightVal[1])
            relationNode = Node(current, rightNode, relation, current.parent, relationShift, '', reverse)
            current.parent = relationNode
            if relationNode.parent != 'Root':
                relationNode.parent.left = relationNode
            current = relationNode
            rightNode.parent = relationNode
        else:
            # Inserts two leaf nodes below the lowest node
            leftNode = Node(None, None, relation, self.current, relationShift)
            rightNode = Node(None, None, rightVal, self.current, rightShift, rightVal[1])
            self.setMultiple(current, left=leftNode, right=rightNode, reverse=reverse)
        
        self.terminated = False
        self.recalibrateNodeIDs()
        
    def recalibrateNodeIDs(self):
        ''' Relabels the ID's of all the nodes.'''
        current = self.first
        counter = 0
        while current.parent != 'Root':
            current.ID = 'L' + str(counter)
            current.parent.right.ID = 'R' + str(counter)
            current = current.parent
            counter += 1
        current.ID = 'L' + str(counter)
        
        
    def checkForSingleNode(self):
        ''' Checks whether or not the tree contains only one node and therefore a single literal number or a register.
            If it does, it returns a move/load instruction for that value. Otherwise it returns a boolean 'False' '''
        
        if self.first.parent == 'Root':
            if type(self.first.value) == str:
                return [['mov', self.treeID, self.first.value]]
            else:
                testVal = self.first.value
                ### The next few lines determine whether or not the number is a valid immediate value for the ARMv7 32bit architecture
                while testVal % 4 == 0 and testVal != 0:                   
                    testVal /= 4
                if testVal < 256:
                    return [['mov', self.treeID, self.first.value]]
                else:
                    return [['ldr', self.treeID, '='+str(self.first.value)]]
        return False
        
    def deleteNode(self, nodeID, recalibrateIDs=True):
        '''Deletes a node in the tree specified by its node ID.
           Restores self.current to its former node object after its finished.
           Returns the deleted node'''
           
        restore = self.current
        
        # Set self.current to the node to be deleted
        current = self.first
        while current.ID[1:] != nodeID[1:]:
            current = current.parent
            # Checks if reach end of tree
            if current == 'Root':
                current = restore
                return False
        
        # Delete node and tie up other attatchments
        if nodeID[0] == 'R':
            del current.parent.right
            current.parent.right = None
        else:
            # Note that by deleting the left node, you also delete the right node
            if current.parent != 'Root':
                current.parent.left = current.left
            if current.left != None:
                current.left.parent = current.parent
            del current.right
            del current
            
            # Relabel all the node IDs
            if recalibrateIDs:
                self.recalibrateNodeIDs()
        
        self.current = restore
        return restore
    
    def setMultiple(self, node, value='@', left='@', right='@', parent='@', shift='@', formerID='@', ID='@', reverse='@'):
        ''' Sets multiple attributes of a node all at once'''
        
        if value != '@':
            node.value = value
        if left != '@':
            node.left = left
        if right != '@':
            node.right = right
        if parent != '@':
            node.parent = parent
        if shift != '@':
            node.shift = shift
        if formerID != '@':
            node.formerID = shift
        if ID != '@':
            node.ID = ID
        if reverse != '@':
            node.reverse = reverse
            
               
    def exportNodesToList(self):
        ''' Exports the tree as a list of lists, with each element in the list corresponding 
            to an instruction in the form
                    [operation, Rd, Rn, Rm, shift]
            where shift includes a comma at the start e.g. 6 -> ', lsl 6'.
            If there is no shift then shift is set to be an empty string'''   
        exportList = []
        current = self.first
        while current.parent != 'Root': 
            current = current.parent
            if current.value == 'mov':
                exportList.append(['mov', self.treeID, current.left.value])
                continue
            if current.left.shift != 0 or current.reversed:
            # Turns it into [operation, Rd, Rn, Rd, shift]
                shift = current.left.shift
                rightVal = str(current.left.value)
                if rightVal[0] != 'r':
                    rightVal = self.treeID
                leftVal = str(current.right.value)
                
            else:
            # Turns it into [operation, Rd, Rd, Rn, shift]
                shift = current.right.shift
                leftVal = str(current.left.value)
                if leftVal[0] != 'r':
                    leftVal = self.treeID
                rightVal = str(current.right.value)
            # Turns the shift into something to the valid form
            if shift > 0:
                shift = ', lsl ' + str(shift)
            elif shift < 0:
                shift = ', lsr ' + str(-1 * shift)
            else:
                shift = ''
            exportList.append([current.value, self.treeID, leftVal, rightVal, shift])
        return exportList
    
    
    def printTree(self):
        ''' Prints a digram of the current state of the tree to the terminal'''
        indent = 0
        restore = self.current
        current = self.first
        def getShift(shift): 
            if shift > 0:
                return ' lsl ' + str(shift)
            elif shift < 0:
                return ' lsr' + str(-1 * shift)
            else:
                return ''
        if current.parent == 'Root':
            print('Single Node = ' + str(current.value) + ' Tree: ' + self.treeID)
            return         
        depth = 0 
        length = len(self.treeID + '|')
        print(self.treeID + ' |')
        print(length*'-' + '+')
        centreVal = str(current.value) + getShift(current.shift)
        while current.parent != 'Root':
            current = current.parent
            leftVal = centreVal
            rightVal = str(current.right.value) + getShift(current.right.shift)
            centreVal = str(current.value) + getShift(current.shift)
            widthL = math.ceil(len(leftVal) /2)
            widthR = math.ceil(len(rightVal) /2) 
            slashNum = max(widthL, widthR) 
            print(str(depth).rjust(length) + '|' + indent*' ' + leftVal + (2*slashNum + len(centreVal) - widthL - widthR+2)*' ' + rightVal)    
            indent +=  widthL
            for count in range(slashNum-1,-1, -1):
                print(length*' ' + '| ' + indent*' ' + '\\' + (len(centreVal)+2*count)*' ' + '/')
                indent += 1
            depth += 1
        print(length*' ' + '| ' + indent*' ' + centreVal)
        self.current = restore

        ## Template for Testing ##
#instructions, labelDict = constantFolding(instructions)
#for i in instructions:
#    print(i)
####################################################################################################
            
                          
instructions, labelDict = insertITBlocks(instructions, labelDict)
instructions, labelDict = unrollingForLoops(instructions, labelDict)
instructions, labelDict = inlineFunctions(instructions, labelDict)
instructions, labelDict = constantFolding(instructions)
for i in instructions:
    print(i)
